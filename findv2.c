//This is the second version of find cmd!
#include <stdio.h>
 #include <stdlib.h>
 #include <strings.h>
 #include <unistd.h>
# include <errno.h>
#include <sys/stat.h>
 #include <sys/types.h> 
 #include <dirent.h> 
# include <pwd.h>
# include <grp.h>
# include <time.h>
void find(const char *name, int level,char *a)
{
	DIR *dir;
	
	char * filePath=name;
    struct dirent *entry;

    if (!(dir = opendir(name)))
        return;
    if (!(entry = readdir(dir)))
        return;

    do {
        if (entry->d_type == DT_DIR) {
            char path[1024];
            int len = snprintf(path, sizeof(path)-1, "%s/%s", name, entry->d_name);
            path[len] = 0;
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            find(path, level + 1,a);
        }
        else
	{
	  struct stat info;
	  char * check=NULL;
	  stat(entry->d_name,&info);
					if (entry->d_type==1) 
			                {
						check="p";				
					}
					else if (entry->d_type==2) 
			                {
						check="c";				
					}
					else if (entry->d_type==4) 
			                {
						check="d";				
					}
					else if (entry->d_type==6) 
			                {
						check="b";				
					}
					else if (entry->d_type==8) 
			                {
						check="-";				
					}
					else if (entry->d_type==10) 
			                {
						check="l";				
					}
					else if (entry->d_type==12) 
			                {
						check="s";				
					}
					
					if((strcmp(a,check))==0)
					{
						printf("/%s\n",entry->d_name);
					}
					
					
	}
            //printf("%*s- %s\n", level*2, "", entry->d_name);
    } while (entry = readdir(dir));
    closedir(dir);
}
 int main(int argc,char *argv[])
{ 
    
    struct dirent *dptr = NULL; 
    unsigned int count = 0; 
	

		   char * curr_dir = argv[1];
		    char * check=argv[2];
		    if(check[0]=='-' && check[1]=='t' && check[2]=='y' && check[3]=='p' && check[4]=='e')
		    {
			
			   char * fileType=argv[3];
			    if(NULL == curr_dir) 
			    { 
				printf("\n ERROR : Could not get the working directory\n"); 
				return -1; 
			    }
			   find(curr_dir,0,fileType);
			  
			    printf("\n"); 
			   
		    }
		    else
		    {
			printf("Incorrect command!\n");
		    }
		
    return 0; 
 }

