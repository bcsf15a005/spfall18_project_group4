//This is the version1 of the find command it deals with the find by name option
#include<unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include<stdbool.h>
#include<dirent.h>
#include <sys/stat.h>
#include<errno.h>
extern int errno;
void find_name(char *  dir,char * name,char * path);
char cwd[1024];
bool all= false;
int main(int argc , char *argv[])
{
	if(argc >= 4)
	{
		if(strcmp(argv[2],"-name")==0)
		{
			find_name(argv[1],argv[3],argv[1]);
		}	
	else
		{
			printf("find: paths must precede expression: %s\n",argv[1]);
			printf("Usage: find [-H] [-L] [-P] [-Olevel] [-D help|tree|search|stat|rates|opt|exec|time] [path...] 					[expression]\n");
			return 0;
		}	
	
	}
return 0;
}
void find_name(char *  dir,char * name,char * path)
{
	struct dirent *entry;
	
	DIR * dp = opendir(dir);
	if(dp==NULL)
	{
		char errorpath[12+strlen(path)];
		strcpy(errorpath,"find: '");
		strcat(errorpath,path);
		strcat(errorpath,"' ");
		strcat(errorpath,"\0");
		perror(errorpath);		
		return;
	}

	chdir(dir);
struct stat info;
	while((entry=readdir(dp))!=NULL)
	{
		if((strcmp(entry->d_name,".")!=0) && (strcmp(entry->d_name,"..")!=0))
		{
			
			if(strcmp(entry->d_name,name)==0)
			{
				printf("%s/%s\n",path,name);
			}
			stat(entry->d_name,&info);
			if((info.st_mode & 0170000)==0040000)
			{
				
				char newpath[strlen(path)+1+1+strlen(entry->d_name)];
				strcpy(newpath,path);
				strcat(newpath,"/");
				strcat(newpath,entry->d_name);
				strcat(newpath,"\0");
				find_name(entry->d_name,name,newpath);
			}
		//	else if(strcmp(entry->d_name,name)==0)
		//	{
		//		printf("%s/%s\n",path,name);
		//	}
		}		
//	}
	closedir(dp);
	chdir("../");
}



	



