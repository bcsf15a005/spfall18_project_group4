#include<stdio.h>
#include<dirent.h>
#include<stdlib.h>
#include<string.h>
#include<sys/ioctl.h>
#include<sys/types.h>
#include<sys/stat.h>


void findv1(char *, char *);
char getType(struct stat);
void findv2(char *, char);
void findv3(char *,char *,char);

int main(int argc, char *argv[])
{
	if (argc != 4)
	{
		printf("Wrong syntax\n");
		exit(0);
	}
	char *nameKey;
	char typeKey;
	for (int i = 0; i < argc; i++)
	{
		if (strcmp(argv[i], "-name") == 0)
		{
			if ((i+1) < argc)
			{
				nameKey = (char *)malloc(sizeof(char)*(1+strlen(argv[i+1])));
				strcpy(nameKey, argv[i+1]);
				if(argc == 4)
				{
					findv1(argv[1], nameKey);
					break;
				}
			}
			else
				exit(0);
		}
		if (strcmp(argv[i], "-type") == 0)
		{
			if ((i+1) < argc)
			{
				typeKey = argv[i+1][0];
				//printf("%c\n",typeKey);
				if(argc == 4)
				{
					findv2(argv[1],typeKey);
					break;
				}
			}
			else
				exit(0);
		}
	}
	if(argc == 6)
	{
		findv3(argv[1],nameKey,typeKey);
	}
	return 0;
}

void findv1(char *Dir, char *nameKey)
{
	DIR *dp = opendir(Dir);
	if (dp == NULL)
	{
		printf("%s", Dir);
		exit(0);
	}

	struct dirent *entry;

	while ((entry = readdir(dp)) != NULL)
	{
		if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
			continue;
		struct stat info;
		char *path = (char*)malloc(sizeof(char)*(strlen(Dir)+strlen(entry->d_name)+2));
		sprintf(path,"%s/%s",Dir,entry->d_name);
		if(stat(path,&info) != -1)
		{
			if (strcmp(entry->d_name, nameKey) == 0)
			{
				printf("%s\n", path);
			}
			if (S_ISDIR(info.st_mode))
			{
				findv1(path,nameKey);
			}
		}
		else
		{
			printf("stat failed");
		}
		free(path);
	}
	closedir(dp);
}

void findv2(char *Dir, char typeKey)
{
	DIR *dp = opendir(Dir);
	if (dp == NULL)
	{
		printf("%s", Dir);
		exit(0);
	}

	struct dirent *entry;

	while ((entry = readdir(dp)) != NULL)
	{
		if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
			continue;
		struct stat info;
		char *path = (char*)malloc(sizeof(char)*(strlen(Dir)+strlen(entry->d_name)+2));
		sprintf(path,"%s/%s",Dir,entry->d_name);
		char typeOfFile;

		if(lstat(path,&info) != -1)
		{
			typeOfFile = getType(info);
			if (typeOfFile == typeKey)
			{
				printf("File or directory found : %s\n", path);
			}
			if (S_ISDIR(info.st_mode))
			{
				findv2(path,typeKey);
			}
		}
		else
		{
			printf("stat failed");
		}
		free(path);
	}
	closedir(dp);
}

void findv3(char *Dir,char* nameKey,char typeKey)
{
	DIR *dp = opendir(Dir);
	if (dp == NULL)
	{
		printf("%s", Dir);
		exit(0);
	}

	struct dirent *entry;

	while ((entry = readdir(dp)) != NULL)
	{
		if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
			continue;
		struct stat info;
		char *path = (char*)malloc(sizeof(char)*(strlen(Dir)+strlen(entry->d_name)+2));
		sprintf(path,"%s/%s",Dir,entry->d_name);
		char typeOfFile;
		
		if(stat(path,&info) != -1)
		{
			typeOfFile = getType(info);
			if ((strcmp(entry->d_name, nameKey) == 0) && (typeOfFile == typeKey))
			{
				printf("File or directory found : %s\n", path);
			}
			if (S_ISDIR(info.st_mode))
			{
				findv3(path,nameKey,typeKey);
			}
		}
		else
		{
			printf("stat failed");
		}
		free(path);
	}
	closedir(dp);
}

char getType(struct stat info)
{
	if (S_ISREG(info.st_mode))
		return 'f';
	else if (S_ISDIR(info.st_mode))
		return 'd';
	else if (S_ISLNK(info.st_mode))
		return 'l';
	else if (S_ISCHR(info.st_mode))
		return 'c';
	else if (S_ISBLK(info.st_mode))
		return 'b';
	else if (S_ISFIFO(info.st_mode))
		return 'f';
	else if (S_ISSOCK(info.st_mode))
		return 's';
}
